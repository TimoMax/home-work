// Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування?
// Інструмент екранування був створений,
// щоб допомогти з екрануванням спеціальних символів Юнікоду в рядкове літеральне значення 
// 

// Які засоби оголошення функцій ви знаєте ?
// Func declaration, Func expression , Arrow func

// Що таке hoisting, як він працює для змінних та функцій ?
// Підняття або hoisting - це механізм JavaScript, в якому змінні і оголошення функцій,
//   пересуваються вгору своєї області видимості перед тим, як код буде виконано.
// Як наслідок, це означає те, що зовсім неважливо де були оголошені функція або змінні, 
// всі вони пересуваються вгору своєї області видимості, незалежно від того, локальна вона або глобальна.

let firstName = prompt("Input user name");
let lastName = prompt("Input user last name");
let birthday = prompt("Input user birthday");


const createNewUser = function (firstName, lastName, birthday) {
  const newUser = {
    firstName,
    lastName,
    birthday,
    getLogin() {
      return newUser.firstName.slice(0, 1).toLowerCase() + newUser.lastName.toLowerCase();
    },

    getPassword() {

      return newUser.firstName.slice(0, 1).toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthday.slice(6);
    },

    getAge() {
      const dd = newUser.birthday.slice(0, 2);
      const mm = newUser.birthday.slice(3, 5) - 1;
      const yyyy = newUser.birthday.slice(6);
      return Math.floor((new Date() - new Date(yyyy, mm, dd)) / ((1000 * 60 * 60 * 24 * 365.25)));
    },
  };
  return newUser;
}
const user = createNewUser(firstName, lastName, birthday);

console.log(`User: ${firstName} ${lastName}`);
console.log(`login:`, user.getLogin());
console.log(`password:`, user.getPassword());
console.log(`age:`, user.getAge());





