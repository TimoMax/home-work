// Опишіть своїми словами що таке Document Object Model(DOM)
// Структура документу (сайту) яка складається з безлічі інших елементів


// Яка різниця між властивостями HTML - елементів innerHTML та innerText ?
// передає HTML - розмітку, передає текст

//   Як можна звернутися до елемента сторінки за допомогою JS ? Який спосіб кращий ?
// document.getElementById
// document.getElementsByClassName
// document.getElementsByTagName
// document.querySelector
// document.querySelectorAll
// Кожен спосіб кращій у своєму випадку відповідно до мети




// /1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const pCollection = document.querySelectorAll('p');
pCollection.forEach((elem) => elem.style.backgroundColor = '#ff0000')


//2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const optionsListiD = document.getElementById('optionsList');

console.log('Знайти елемент з id="optionsList":', optionsListiD);
console.log('Знайти батьківський елемент', optionsListiD.parentElement);
console.log('Знайти дочірні ноди', optionsListiD.childNodes);


//3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - 'This is a paragraph'
const pParagraph = document.getElementById('testParagraph');
pParagraph.textContent = 'This is a paragraph';


//4) Отримати елементи, вкладені в елемент із класом main - header і вивести їх у консоль.Кожному з елементів присвоїти новий клас nav - item.
const mainHeaderRename = document.querySelectorAll('.main-header li');
mainHeaderRename.forEach((elem) => elem.className = 'nav-item')
console.log(mainHeaderRename);



//5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const sectionTitleRemove = document.querySelectorAll('.section-title');
sectionTitleRemove.forEach((elem) => elem.classList.remove('section-title'));
console.log(sectionTitleRemove);  