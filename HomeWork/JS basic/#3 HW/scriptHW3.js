// Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
// для уникання повторюванності коду у разі багаторазового повторення однакових операцій
// Це значно оптимізує код і робить його лаконічним
// -----------------------------------------------------------

// Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
// Коли одну і ту ж змінну треба перезаписати з певним кроком (і++)

// -----------------------------------------------------------

// Що таке явне та неявне приведення(перетворення) типів даних у JS ?
// Явне перетворення - коли ми навмисно змінюємо тип. Наприклад унарний оператор + перед  prompt
// не явне перетворення - коли перетворення робить браузер слідуючі своїм власним алгоритмам. Наприклад "5" * 5 = 25.
// у цьому випадку відбувається неявне перетворення першого операнду до типу number

// -----------------------------------------------------------


// Завдання
// let enyNumber = +prompt("Input any number");
// for (let i = 0; i < enyNumber + 1; i++){
//   if (enyNumber < 5) {
//     alert("Sorry, no numbers");
//     break;
//   } else if (i % 5 === 0) {
//     console.log(i);
//   }
// }


// Необов'язкове завдання підвищеної складності
let countNum = null;
let firstNumber = +prompt("Input first number");
while (Number.isInteger(firstNumber) === false) {
  firstNumber = +prompt("Input first number", firstNumber)
}
let secondNumber = +prompt("Input second number");
while (Number.isInteger(secondNumber) === false) {
  secondNumber = +prompt("Input first number", secondNumber)
}

let maxNum = Math.max(firstNumber, secondNumber);
let minNum = Math.min(firstNumber, secondNumber);

console.log(`number range from ${minNum} to ${maxNum}`);

for (let i = minNum; i < maxNum + 1; i++) {
  for (let j = 2; j <= i; j++) {
    if (i % j === 0 && j < i) {
      break;
    } else if (j === i) {
      countNum++;
      console.log(i);
    }
  }
}
if (countNum === null) {
  console.log("Sorry no numbers");
  alert("Sorry no numbers");
};
console.log("count Numbers:", countNum);
