const tabs = document.querySelectorAll('.tabs-title');
const tabHeads = document.querySelector('.tabs');
const tabContent = document.querySelectorAll('.tabs-item');

tabHeads.addEventListener('click', function (event) {
  const currentDataElem = event.target.getAttribute("data-tab");
  const currentTabItem = document.getElementById(currentDataElem);

  tabs.forEach(element => {
    element.classList.remove('active');
    currentTabItem.classList.remove('active');

  });

  tabContent.forEach(element => {
    element.classList.remove('active');
  });

  event.target.classList.add('active')
  currentTabItem.classList.add('active');
});

