const btnChanger = document.getElementById('btn');
let themeHolder = document.getElementById('themeHolder');
let theme = localStorage.getItem('href');
if (theme === null) {
  themeHolder.setAttribute('href', "style/style.css");
} else {
  themeHolder.setAttribute('href', theme)
}

btnChanger.addEventListener('click', () => {
  if (themeHolder.getAttribute('href') == "style/style.css") {
    themeHolder.setAttribute('href', "style/style — копия.css");
    localStorage.setItem('href', "style/style — копия.css");
    theme = localStorage.href
  } else {
    themeHolder.setAttribute('href', "style/style.css")
    localStorage.setItem('href', "style/style.css");
    theme = localStorage.href
  }
});