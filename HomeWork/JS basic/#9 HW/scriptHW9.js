// Опишіть, як можна створити новий HTML тег на сторінці.
// const x = document.createElement('tag');
// document.body.append(x)
// document.body.prepend(x)

// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// перший параметр функції insertAdjacentHTML - це місце вставки елементу
// 'beforebegin'
// 'afterbegin'
// 'beforeend'
// 'afterend'

// Як можна видалити елемент зі сторінки ?
// за допомогою Element.remove()


const arr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnipro"]
const listGenerating = (arr, place = document.body) => {
  const firstLevelList = document.createElement('ul');

  arr.forEach(e => {

    if (Array.isArray(e)) {
      listGenerating(e, firstLevelList)
    } else {
      let listElem = document.createElement('li');
      listElem.innerHTML = e;
      firstLevelList.append(listElem);
    }
  });
  place.append(firstLevelList);
}
listGenerating(arr);



clearPageWithTimer = (sec) => {
  const timerDiv = document.createElement('div');
  document.body.append(timerDiv);

  let timeLeft = sec;
  const intervalId = setInterval(() => {
    timeLeft--;
    timerDiv.innerText = `Очистка через ${timeLeft} секунд`;

    if (timeLeft === 0) {
      clearInterval(intervalId);
      document.body.innerHTML = '';
    }
  }, 1000);
}

setTimeout(() => {
  clearPageWithTimer(3);
});

