// Чому для роботи з input не рекомендується використовувати клавіатуру ?
// існують різні типи введення, і різні розкладки клавіатури у яких кнопки мають різний key.event

const btns = document.querySelectorAll('.btn');

window.addEventListener('keydown', (event) => {
  btns.forEach(elem => {
    elem.classList.remove('active');
    if (event.key.toLowerCase() === elem.textContent.toLowerCase()) {
      elem.classList.add('active');
    };
  })
});
