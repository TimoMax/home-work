const form = document.querySelector('.password-form');
const pass1 = document.getElementById('pass1');
const pass2 = document.getElementById('pass2');
const btn = document.getElementsByTagName('button');
const error = document.querySelector('h3');

// порівняння і алерти
form.addEventListener('click', function (event) {
  const currentElement = event.target;

  if (currentElement.classList.contains('btn')) {
    if (pass1.value === pass2.value) {
      error.style.opacity = 0;
      alert("You are welcome");
    } else {
      error.style.opacity = 1;
    }
  }

  // логіка

  if (currentElement.classList.contains('icon-password')) {
    const passFirstOrSecond = currentElement.dataset.id;
    const inputPass = document.getElementById(passFirstOrSecond);

    if (currentElement.classList.contains('fa-eye')) {
      currentElement.classList.add('fa-eye-slash')
      currentElement.classList.remove('fa-eye')
      inputPass.setAttribute('type', 'text');

    } else {
      currentElement.classList.add('fa-eye')
      currentElement.classList.remove('fa-eye-slash')
      inputPass.setAttribute('type', 'password');
    }
  }

})





