// 1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval(`)`.
// setTimeout() - одна відкладена дія/дії
// setInterval(`) - одна відкладена дія/дії яка(і) багаторазово повторюється

// 2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку ? Чи спрацює вона миттєво і чому ?
// ні. не спрацює. по дефолту мінімальна затримка складає 4 мілісекунди

// 3. Чому важливо не забувати викликати функцію`clearInterval()`, коли раніше створений цикл запуску вам вже не потрібен ?
// Бо сама функція, а особливо змінні на які вона посилаєтсья продовжуть займати пам'ять браузера.



const imgCollection = document.querySelectorAll('.image-to-show');
const stopBtn = document.querySelector('.stop');
const startBtn = document.querySelector('.start');
const paragraph = document.querySelector('p');

let timer = null;
let imgIndex = 0;


const slideShow = () => {
  imgCollection.forEach((elem) => {
    elem.classList.remove('active');
  });
  imgIndex++;
  if (imgIndex >= imgCollection.length) {
    imgIndex = 0;
  }
  imgCollection[imgIndex].classList.add('active');
}



stopBtn.addEventListener('click', () => {
  clearInterval(timer)
  timer = null;
});

timer = setInterval(slideShow, 3000);

startBtn.addEventListener('click', () => {
  timer = setInterval(slideShow, 3000);
});




// startBtn.addEventListener('click', () => {

//   timer = setInterval(() => {
//     imgCollection[imgIndex].classList.add('active');
//     sec--;
//         paragraph.innerText = sec;
//     if (sec <= 0) {
//       sec = 3;
//       imgCollection.forEach((elem) => {
//         elem.classList.remove('active');
//       })
//       imgIndex++;
//       if (imgIndex >= imgCollection.length) {
//         imgIndex = 0;
//       }
//     }
//   }, 1000)
// })

// stopBtn.addEventListener('click', () => {
//   clearInterval(timer)
// });