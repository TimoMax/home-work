// tabs

const tabs = document.querySelectorAll('.tabs-title');
const tabHeads = document.querySelector('.tabs');
const tabContent = document.querySelectorAll('.tabs-item');

tabHeads.addEventListener('click', function (event) {
  const currentDataElem = event.target.getAttribute("data-tab");
  const currentTabItem = document.getElementById(currentDataElem);

  tabs.forEach(element => {
    element.classList.remove('active');
    currentTabItem.classList.remove('active');

  });

  tabContent.forEach(element => {
    element.classList.remove('active');
  });

  event.target.classList.add('active')
  currentTabItem.classList.add('active');
});

// --------------------------------------------------Our amasing work-----------------------------------------------

const photoGalery = [
  {
    src: './Images/OurAmazingWork/graphic-design (1).jpg',
    data: "gd",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (2).jpg',
    data: "wd",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (3).jpg',
    data: "lp",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (4).jpg',
    data: "wp",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (5).jpg',
    data: "gd",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (6).jpg',
    data: "wd",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (7).jpg',
    data: "lp",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (8).jpg',
    data: "wp",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (9).jpg',
    data: "gd",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (10).jpg',
    data: "wd",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (11).jpg',
    data: "lp",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (12).jpg',
    data: "wp",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (13).jpg',
    data: "gd",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (14).jpg',
    data: "wd",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (15).jpg',
    data: "lp",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (16).jpg',
    data: "wp",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (17).jpg',
    data: "gd",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (18).jpg',
    data: "wd",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (19).jpg',
    data: "lp",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (20).jpg',
    data: "wp",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (21).jpg',
    data: "gd",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (22).jpg',
    data: "wd",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (23).jpg',
    data: "lp",
  },
  {
    src: './Images/OurAmazingWork/graphic-design (24).jpg',
    data: "wp",
  },
  // {
  //   src: './graphic_design/graphic-design (25).jpg',
  //   data: "gd",
  // },
  // {
  //   src: './graphic_design/graphic-design (26).jpg',
  //   data: "wd",
  // },
  // {
  //   src: './graphic_design/graphic-design (27).jpg',
  //   data: "lp",
  // },
  // {
  //   src: './graphic_design/graphic-design (28).jpg',
  //   data: "wp",
  // },
  // {
  //   src: './graphic_design/graphic-design (29).jpg',
  //   data: "gd",
  // },
  // {
  //   src: './graphic_design/graphic-design (30).jpg',
  //   data: "wd",
  // },
  // {
  //   src: './graphic_design/graphic-design (31).jpg',
  //   data: "lp",
  // },
  // {
  //   src: './graphic_design/graphic-design (32).jpg',
  //   data: "wp",
  // },
  // {
  //   src: './graphic_design/graphic-design (33).jpg',
  //   data: "gd",
  // },
  // {
  //   src: './graphic_design/graphic-design (34).jpg',
  //   data: "wd",
  // },
  // {
  //   src: './graphic_design/graphic-design (35).jpg',
  //   data: "lp",
  // },
  // {
  //   src: './graphic_design/graphic-design (36).jpg',
  //   data: "wp",
  // },
  // {
  //   src: './graphic_design/graphic-design (37).jpg',
  //   data: "gd",
  // },
  // {
  //   src: './graphic_design/graphic-design (38).jpg',
  //   data: "wd",
  // },
  // {
  //   src: './graphic_design/graphic-design (39).jpg',
  //   data: "lp",
  // },
  // {
  //   src: './graphic_design/graphic-design (40).jpg',
  //   data: "wp",
  // },
  // {
  //   src: './graphic_design/graphic-design (41).jpg',
  //   data: "gd",
  // },
  // {
  //   src: './graphic_design/graphic-design (42).jpg',
  //   data: "wd",
  // },
  // {
  //   src: './graphic_design/graphic-design (43).jpg',
  //   data: "lp",
  // },
  // {
  //   src: './graphic_design/graphic-design (44).jpg',
  //   data: "wp",
  // },
  // {
  //   src: './graphic_design/graphic-design (45).jpg',
  //   data: "gd",
  // },
  // {
  //   src: './graphic_design/graphic-design (46).jpg',
  //   data: "wd",
  // },
  // {
  //   src: './graphic_design/graphic-design (47).jpg',
  //   data: "lp",
  // },
  // {
  //   src: './graphic_design/graphic-design (48).jpg',
  //   data: "wp",
  // },

]

const ul = document.getElementById('galery-inserter');
const btnPhotoFilter = document.querySelector('.tabs2');
const loadBatton = document.querySelector('.load-btn')
const containerExtention = 1700;
const containerAsItIs = 1212;

loader = (a, b) => {
  setTimeout(() => {
    for (let index = a; index <= b; index++) {
      const element = photoGalery[index];
      let li = document.createElement('li');
      let div = document.createElement('div');
      div.classList.add("abc");
      div.innerHTML = `<img class="js-galery-items" src="${element.src}" alt="image.jpg ">
    <div class="js-another-side-wrapper">
                  <div class="another-side-content">
                    <div class="links-img">
                      <a href="">
                        <div class="chain">
                          <div class="chain-svg">
                            <svg width="15" height="15" viewBox="0 0 15 15" fill="none"
                              xmlns="http://www.w3.org/2000/svg">
                              <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"
                                fill="#1FDAB5" />
                            </svg>
                          </div>
                      </a>
                    </div>
                    <a href="">
                      <div class="square-in-circle">
                        <div class="square">
                        </div>
                      </div>
                    </a>
                  </div>
                  <p class="another-side-p1">CREATIV DESIGN</p>
                  <p class="another-side-p2">Web Design</p>
                </div>`;
      ul.append(li);
      li.append(div);
    };
  }, 100);

};

containerManager = (a) => {
  const extendedContainer = document.querySelector('.our-amazing-work');
  extendedContainer.setAttribute("style", `height: ${a}px`);
  loadBatton.setAttribute('style', "opacity: 0");
};

setTimeout(() => {
  loader(0, 11)
}, 100);
// ===========

// load-Button ====
loadBatton.addEventListener('click', () => {
  setTimeout(() => {
    loader(12, 23)
    containerManager(containerExtention);
  }, 100);

})
// Filter ======

btnPhotoFilter.addEventListener('click', (event) => {
  let liForRemove = document.querySelectorAll('.js-galery-items');
  liForRemove.forEach(element => element.remove())
  containerManager(containerAsItIs);
  let currentPhotoData;
  setTimeout(() => {
    currentPhotoData = event.target.getAttribute("data-tab");
    let filterPhotoById = photoGalery.filter(item => item.data == currentPhotoData);
    filterPhotoById.forEach(function (element) {
      let li = document.createElement('li');
      let div = document.createElement('div');
      div.classList.add("abc");
      div.innerHTML = `<img class="js-galery-items" src="${element.src}" alt="image.jpg ">
    <div class="js-another-side-wrapper">
                  <div class="another-side-content">
                    <div class="links-img">
                      <a href="">
                        <div class="chain">
                          <div class="chain-svg">
                            <svg width="15" height="15" viewBox="0 0 15 15" fill="none"
                              xmlns="http://www.w3.org/2000/svg">
                              <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"
                                fill="#1FDAB5" />
                            </svg>
                          </div>
                      </a>
                    </div>
                    <a href="">
                      <div class="square-in-circle">
                        <div class="square">
                        </div>
                      </div>
                    </a>
                  </div>
                  <p class="another-side-p1">CREATIV DESIGN</p>
                  <p class="another-side-p2">Web Design</p>
                </div>`;
      ul.append(li);
      li.append(div);

    });

  }, 500);
  currentPhotoData = event.target.getAttribute("data-tab");
  if (currentPhotoData == "all") {
    setTimeout(() => {
      loader(0, 23)
      containerManager(containerExtention);
    }, 100);
  }
});

// -----------------------------------------------------slider-------------------------------------------------------

const mainPhoto = document.querySelector('.js-choosed-photo');
const nextBtn = document.querySelector('.js-btn-plus');
const previousBtn = document.querySelector('.js-btn-minus');
const employePhoto = document.querySelectorAll('.js-employe-photo');
const employeName = document.querySelector('.js-employe-name');
const employeTitle = document.querySelector('.js-employe-title');
const employeSays = document.querySelectorAll('.js-what-people-say-container-text');





let indexEmpoye = 0;

let inserter = () => {
  mainPhoto.setAttribute('src', employePhoto[indexEmpoye].getAttribute('src'));
  employeName.innerText = employePhoto[indexEmpoye].getAttribute('name').toLocaleUpperCase();
  employeTitle.innerText = employePhoto[indexEmpoye].getAttribute('title');
  employePhoto.forEach(element => element.classList.remove('active'));
  employePhoto[indexEmpoye].classList.add('active');
  
  employeSays.forEach(element => element.classList.remove('active'));
  employeSays[indexEmpoye].classList.add('active');

}

nextBtn.addEventListener('click', () => {
  indexEmpoye++;
  if (indexEmpoye > employePhoto.length - 1) { indexEmpoye = 0 }
  inserter()
});


previousBtn.addEventListener('click', () => {
  indexEmpoye--;
  if (indexEmpoye < 0) { indexEmpoye = 3 }
  inserter()
});

document.addEventListener('click', (event) => {
  const currentElement = event.target;
  if (currentElement.classList.contains('js-employe-photo')) {
    mainPhoto.setAttribute('src', currentElement.getAttribute('src'));
    employeName.innerText = currentElement.getAttribute('name').toLocaleUpperCase();
    employeTitle.innerText = currentElement.getAttribute('title');
    employePhoto.forEach(element => element.classList.remove('active'));
    currentElement.classList.add('active');
    currentId = currentElement.getAttribute('id') * 1;
    indexEmpoye = currentId;
    employeSays.forEach(element => element.classList.remove('active'));
    employeSays[indexEmpoye].classList.add('active');

  };
});




