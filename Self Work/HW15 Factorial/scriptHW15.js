// Напишіть, як ви розумієте рекурсію.Навіщо вона використовується на практиці ?
// Коли функція візиває сама себе



// let initialNum = prompt('Enter a number > 0');
// while (initialNum == 0 || isNaN(+initialNum) || initialNum === " ") {
//   initialNum = prompt('Enter a number', initialNum);
// };
// let factorialN = 1;
// for (let index = 1; index <= initialNum; index++) {
//   factorialN *= index;
// };
// console.log(`${initialNum}! = `, factorialN);
// document.write(`${initialNum}! = `, factorialN);


let initialNum = prompt('Enter a number > 0');
const validationNum = initialNum => {
  while (initialNum == 0 || isNaN(+initialNum) || initialNum === " ") {
    initialNum = prompt('Enter a number', initialNum);
  };
  return initialNum;
}
let validNum = validationNum(initialNum);

const calculationFactorial = validNum => {
  let factorialN = 1;
  for (let index = 1; index <= validNum; index++) {
    factorialN *= index;
  };
  return factorialN;
}

let factorialResult = calculationFactorial(validNum);

console.log(`${validNum}! = `, factorialResult);
document.write(`${validNum}! = `, factorialResult);



